<?php
/***************************
Staff config file
***************************/
if (!defined('BOOTSTRAP')) { die('Access denied'); }

// Upload subdirectory
define('STAFF_IMAGE_DIR', 'staff');