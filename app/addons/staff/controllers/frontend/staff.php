<?php
/***************************
 Staff frontend controller
***************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    if ( $mode == 'get_email' && !empty($_REQUEST['staff_id']) ) {
    	// Ajax email request
    	$email = db_get_field("SELECT IF(?:staff.email='' AND ?:users.email IS NOT NULL,?:users.email,?:staff.email) AS email FROM ?:staff LEFT JOIN ?:users ON ?:users.user_id=?:staff.user_id WHERE ?:staff.staff_id=?i", $_REQUEST['staff_id']);
    	// Generate email link
        echo empty($email) ? __('no') : "<a href='mailto:{$email}'>{$email}</a>";
    }
}

exit;