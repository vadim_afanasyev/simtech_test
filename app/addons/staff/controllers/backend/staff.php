<?php
/***************************
 Контроллер staff.php
***************************/
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	if ( $mode == 'update' ) {
		// Update single records
		if ( !empty($_REQUEST['staff_data']) && is_array($_REQUEST['staff_data']) ) {
			fn_staff_update($_REQUEST['staff_data'], isset($_REQUEST['staff_id']) ? $_REQUEST['staff_id'] : 0 );
		}
	}
	
	if ( $mode == 'm_update' ) {
		// Update multiple records
		if ( !empty($_REQUEST['staff_data']) && is_array($_REQUEST['staff_data']) ) {
			foreach ( $_REQUEST['staff_data'] as $pk => $data ) {
				fn_staff_update($data, $pk);
			}		
		}
	}
	
	if ( $mode == 'm_delete' ) {
		// Delete multiple records
		if ( isset($_REQUEST['staff_ids']) ) {
			fn_staff_delete($_REQUEST['staff_ids']);
		}
	}
	
	if ( $mode == 'delete' ) {
		// Delete single record
		if ( !empty($_REQUEST['staff_id']) ) {
			fn_staff_delete($_REQUEST['staff_id']);
		}
	}
	
	return array(CONTROLLER_STATUS_OK, "staff.manage");
}

if ($mode == 'manage') {
	$params = $_REQUEST;

	list($staff, $search) = fn_staff_get_list($params, Registry::get('settings.Appearance.admin_elements_per_page'));
	$maxPos = db_get_field("SELECT MAX(orderby) FROM ?:staff");
	
	Tygh::$app['view']->assign('max_pos', intval($maxPos) + 10);
	Tygh::$app['view']->assign('staff', $staff);
	Tygh::$app['view']->assign('search', $search);
}

if ( $mode == 'update' ) {
	// Edit form
	$staff_id = $_REQUEST['staff_id'];
	$staff = fn_staff_get_data($staff_id);
	
	Tygh::$app['view']->assign('staff', $staff);
}