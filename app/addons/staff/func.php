<?php
/***************************
 Staff functions
***************************/
if (!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Navigation\LastView;
use Tygh\Storage;
use Tygh\db_query;

/**
 * Get staff list
 * 
 * @param array $params
 * @param number $items_per_page
 * @return array($staff, $params)
 */
function fn_staff_get_list($params = array(), $items_per_page = 0) {
	// Init filter
	$params = LastView::instance()->update('staff', $params);

	$default_params = array(
		'page' => 1,
		'items_per_page' => $items_per_page
	);

	$params = array_merge($default_params, $params);
	
    // Init field list
	$fields = array(
		'?:staff.staff_id',
		'CONCAT(IF(?:staff.first_name="" AND ?:users.firstname IS NOT NULL,?:users.firstname, ?:staff.first_name), " ", IF(?:staff.last_name="" AND ?:users.lastname IS NOT NULL, ?:users.lastname, ?:staff.last_name)) as name',
		'IF(?:staff.email="" AND ?:users.email IS NOT NULL,?:users.email,?:staff.email) as email',
		'?:staff.function',
		'?:staff.user_id',
		'?:staff.orderby',
		'?:staff.photo',
	);

	// Define sort fields
	$sortings = array (
		'name' => 'name',
		'email' => '?:staff.email',
		'function' => '?:staff.function',
		'user' => '?:staff.user_id',
		'orderby' => '?:staff.orderby',
	);

	$joins = array('LEFT JOIN ?:users ON ?:users.user_id = ?:staff.user_id');
	$conditions = fn_staff_build_conditions($params);
	$sorting = db_sort($params, $sortings, 'orderby', 'asc');
	$group = '';

	$limit = '';
	if (!empty($params['limit'])) {
		$limit = db_quote(' LIMIT 0, ?i', $params['limit']);
	} elseif (!empty($params['items_per_page'])) {
		$params['total_items'] = db_get_field("SELECT COUNT(DISTINCT(?:staff.staff_id)) FROM ?:staff WHERE 1 ?p", $conditions);
		$limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
	}
	
	$staff = db_get_hash_array(
		"SELECT " . implode(', ', $fields) . " "
		. "FROM ?:staff " . implode(' ', $joins) . " WHERE 1 ?p $group $sorting $limit",
		'staff_id', $conditions
	);
	
	if ( !empty($params['extractPath']) ) {
		foreach ( $staff as $k => $v ) {
			$staff[$k]['photo_data'] = fn_staff_get_photo_data($v['photo']);
		}
	}

	return array($staff, $params);
}

/**
 * Return all staff for blocks
 * 
 * @return array()
 */
function fn_staff_get_block() {
	list($staff, $search) = fn_staff_get_list(array(
		'limit' => 9999999,       // Return all data
		'existImages' => true,    // Only with images
		'extractPath' => true,    // Extract image path
	));
	
	return $staff;
}

/**
 * Create staff condition
 * 
 * @param array $params
 * @return string $conditions
 */
function fn_staff_build_conditions(array $params) {
	$conditions = '';
	
	if ( !empty($params['existImages']) ) {
		$conditions .= db_quote(" AND ?:staff.photo IS NOT NULL");
	}
	
	return $conditions;
}

/**
 * Update or create new staff data
 * 
 * @param array $data
 * @param number $staff_id
 * @return number
 */
function fn_staff_update($data, $staff_id = 0) {
	// check if such tag is exists
	$existing_id = db_get_field("SELECT staff_id FROM ?:staff WHERE staff_id = ?i", $staff_id);
	
	if ( ( $photo = fn_staff_upload_photo('upload') ) !== false ) {
		$data['photo'] = $photo; 
	}
	
	// Remove photo on exists record
	if ( empty($data['photo']) ) {
		fn_staff_remove_photo($existing_id);
		$data['photo'] = null;
	}
	
	if ( $staff_id > 0 && $staff_id == $existing_id ) {
		// Update data
		db_query("UPDATE ?:staff SET ?u WHERE staff_id = ?i", $data, $existing_id);
		$staff_id = $existing_id;
	} else {
		// New Data
		$staff_id = db_query("INSERT INTO ?:staff ?e", $data);
	}
	
	return $staff_id;
}

/**
 * Get staff single data
 * 
 * @param number $staff_id
 */
function fn_staff_get_data($staff_id) {
	$staff = db_get_row("SELECT * FROM ?:staff WHERE staff_id = ?i", $staff_id);
	
	if ( !empty($staff) ) {
		$staff['photo_data'] = fn_staff_get_photo_data($staff['photo']);
	}
	
	return $staff;
}

/**
 * Extrach photo data
 * 
 * @param string $photo
 * @return array or null if failed
 */
function fn_staff_get_photo_data($photo) {
	if ( !empty($photo) ) {
		return array(
			'http_photo_path' => Storage::instance('images')->getUrl(STAFF_IMAGE_DIR . '/' . $photo, 'http'),
			'https_photo_path' => Storage::instance('images')->getUrl(STAFF_IMAGE_DIR . '/' . $photo, 'https'),
		);
	} else {
		return null;
	}
}

/**
 * Delete staff by pk
 * 
 * @param number or array $pk
 */
function fn_staff_delete($staff_ids) {
	return db_query("DELETE FROM ?:staff WHERE staff_id IN(?n)", is_array($staff_ids) ? $staff_ids : array($staff_ids));
}

/**
 * Upload photo
 * 
 * @return string or false
 */
function fn_staff_upload_photo($name) {
	if ( !empty($_FILES['staff_data']) ) {
		$data = $_FILES['staff_data'];
        
        if ( !isset($data['error'][$name]) || $data['error'][$name] != 0 ) {
            // Error occurs
            return false;
        }
	
        // Save uploaded file
		list($images_size, $images_path) = Storage::instance('images')->put(STAFF_IMAGE_DIR . '/' . $data['name'][$name], array(
			'file' => $data['tmp_name'][$name]
		));

        return $data['name'][$name];
	}

	return false;
}

/**
 * Remove photo from FS
 * 
 * @param number $staff_id
 * @return boolean
 */
function fn_staff_remove_photo($staff_id) {
	if ( intval($staff_id) <= 0 ) {
		// Incorrect pk
		return false;
	}
	
	$photo = db_get_field("SELECT photo FROM ?:staff WHERE staff_id = ?i", $staff_id);
	
	if ( !empty($photo) ) {
		return Storage::instance('images')->delete(STAFF_IMAGE_DIR . '/' . $photo);
	}
	
	return false;
}