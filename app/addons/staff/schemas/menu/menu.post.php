<?php
/***************************
Тестовый модуль: Добавление в меню
***************************/

$schema['central']['website']['items']['staff_members'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'href' => 'staff.manage',
    'position' => 230
);

return $schema;