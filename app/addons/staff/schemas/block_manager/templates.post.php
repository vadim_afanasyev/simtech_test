<?php
$schema['addons/staff/blocks/staff_list.tpl'] = array(
	'settings' => array(
		'not_scroll_automatically' => array (
			'type' => 'checkbox',
			'default_value' => 'Y'
		),
		'scroll_per_page' =>  array (
			'type' => 'checkbox',
			'default_value' => 'N'
		),
		'speed' =>  array (
			'type' => 'input',
			'default_value' => 400
		),
		'pause_delay' =>  array (
			'type' => 'input',
			'default_value' => 3
		),
		'item_quantity' =>  array (
			'type' => 'input',
			'default_value' => 6
		),
		'thumbnail_width' =>  array (
			'type' => 'input',
			'default_value' => 192
		),
		'outside_navigation' => array (
			'type' => 'checkbox',
			'default_value' => 'Y'
		)
	),
);

return $schema;