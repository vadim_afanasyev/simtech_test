<?php
use Tygh\Registry;

$schema['staff'] = array(
	'templates' => array(
		'addons/staff/blocks/staff_list.tpl' => array(),
	),
	'wrappers' => 'blocks/wrappers',
    'content' => array(
    	'staff_list' => array(
			'type' => 'function',
			'function' => array('fn_staff_get_block'),
		),
	),
	// Enable block caching
	'cache' => array(
		'update_handlers' => array('staff')
	),	
);

return $schema;