<script type="text/javascript">
//[[CDATA
$(function() {
	$('.staff-wrapper .email a').on('click', function(e) {
		e.preventDefault();
		
		if ( $(this).hasClass('data-loading') ) {
			return;
		}
		
		var id = $(this).attr('staff-id');
		var that = $(this);
		var container = $(this).parent();
	
		$.ajax({
			type: 'post',
			url: 'index.php?dispatch=staff.get_email&staff_id=' + id,
			beforeSend: function() {
				$(that).addClass('data-loading');
				return true;
			},
			success: function(html) {
				$(container).html(html);
			},
			error: function() {
				alert('Ops, error');
			},
			complete: function() {
				$(that).removeClass('data-loading');
			}
		});
	});
});
//]]
</script>