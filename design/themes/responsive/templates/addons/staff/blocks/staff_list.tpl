{if $staff_list}
{$obj_prefix = "staff_`$block.block_id`"}

<div class="staff-wrapper">
	{if $block.properties.outside_navigation == "Y"}
	<div class="staff-owl-theme owl-controls">
		<div class="owl-controls clickable owl-controls-outside" id="owl_outside_nav_{$block.block_id}">
			<div class="owl-buttons">
				<div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="bx-next"></i></div>
				<div id="owl_next_{$obj_prefix}" class="owl-next"><i class="bx-prev"></i></div>
			</div>
		</div>
	</div>
	{/if}

	<div id="scroll_list_{$block.block_id}" class="owl-carousel">
		{foreach from=$staff_list item="item" name="for_staff_list"}
			<div class="ty-center">
				<div><img src="{$item.photo_data.http_photo_path}" width="{$block.properties.thumbnail_width}" border="0" class="ty-pict lazyOwl" /></div>
				<div><h4>{$item.name}</h4></div>
				<div>{$item.function}</div>
				<div class="ty-mtb-s email"><a staff-id="{$item.staff_id}">{__("show_email")}</a></div>
			</div>
		{/foreach}
	</div>
</div>

{include file="common/scroller_init.tpl" items=$staff_list prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}
{/if}