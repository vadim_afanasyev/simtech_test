{capture name="add_staff"}
{include file="addons/staff/views/staff/update.tpl" max_pos=$max_pos}
{/capture}

{capture name="mainbox"}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}

<form class="form-horizontal form-edit" action="{""|fn_url}" method="post" name="staff_form">
{include file="common/pagination.tpl" save_current_page=true save_current_url=true}
{if $staff}
<table width="100%" class="table table-sort table-middle">
<thead>
<tr>
    <th class="left" width="1%">{include file="common/check_items.tpl"}</th>
    <th width="22%"><a class="cm-ajax{if $search.sort_by == "name"} sort-link-{$search.sort_order_rev}{/if}" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("staff_name")}</a></th>
    <th width="22%"><a class="cm-ajax{if $search.sort_by == "email"} sort-link-{$search.sort_order_rev}{/if}" href="{"`$c_url`&sort_by=email&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("email")}</a></th>
    <th width="22%"><a class="cm-ajax{if $search.sort_by == "function"} sort-link-{$search.sort_order_rev}{/if}" href="{"`$c_url`&sort_by=function&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("staff_function")}</a></th>
    <th width="22%"><a class="cm-ajax{if $search.sort_by == "user"} sort-link-{$search.sort_order_rev}{/if}" href="{"`$c_url`&sort_by=user&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("user")}</a></th>
    <th width="11%"><a class="cm-ajax{if $search.sort_by == "orderby"} sort-link-{$search.sort_order_rev}{/if}" href="{"`$c_url`&sort_by=orderby&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("position")}</a></th>
    <th>&nbsp;</th>
</tr>
</thead>
<tbody>
{foreach from=$staff item="item"}
<tr>
	<td class="left"><input type="checkbox" class="cm-item" value="{$item.staff_id}" name="staff_ids[]"/></td>
	<td><span>{$item.name}</span></td>
	<td><span>{if $item.email}{$item.email}{else}{__("no")}{/if}</span></td>
	<td><input type="text" name="staff_data[{$item.staff_id}][function]" value="{$item.function}" size="20" class="input-hidden"></td>
	{if $item.user_id}
		<td>{btn type="list" text=$item.user_id href="profiles.update?user_id=`$item.user_id`" target_id="content_group`$item.user_id`" prefix=$item.user_id}</td>
	{else}
		<td>{__("no")}</td>
	{/if}
	<td><input type="text" name="staff_data[{$item.staff_id}][orderby]" value="{$item.orderby}" size="10" class="input-hidden"></td>
	
	<td>
		<div class="hidden-tools">
			{capture name="tools_list"}
				<li>{btn type="dialog" title=__("staff_edit_page") text=__("edit") href="staff.update?staff_id=`$item.staff_id`" id="opener_group_`$item.staff_id`" target_id="content_group`$item.staff_id`" prefix=$item.staff_id}</li>
				<li>{btn type="list" class="cm-confirm cm-post" text=__("delete") href="staff.delete?staff_id=`$item.staff_id`"}</li>
			{/capture}
			{dropdown content=$smarty.capture.tools_list}
		</div>
	</td>
</tr>
{/foreach}
</tbody>
</table>
{else}
	<p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl"}
</form>

{/capture}

{capture name="buttons"}

    {capture name="tools_list"}
        {if $staff}
            <li>{btn type="delete_selected" dispatch="dispatch[staff.m_delete]" form="staff_form"}</li>
        {/if}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}

    {if $staff}
        {include file="buttons/save.tpl" but_name="dispatch[staff.m_update]" but_role="submit-link" but_target_form="staff_form"}
    {/if}
    
{/capture}

{capture name="adv_buttons"}
    {include file="common/popupbox.tpl" id="add_staff" text=__("add_staff") title=__("add_new") content=$smarty.capture.add_staff act="general" icon="icon-plus"}
{/capture}

{capture name="sidebar"}
sidebar
{/capture}

{include file="common/mainbox.tpl" title=__("staff_members") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons sidebar=""}