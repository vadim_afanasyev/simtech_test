{if $staff }
	{$id = $staff.staff_id}
{else}
	{$id=0}
{/if}

{$hide_inputs = !""|fn_allow_save_object:"staff"}

<form action="{""|fn_url}" method="post" name="staff_add_form" class="form-horizontal form-edit{if !""|fn_allow_save_object:"staff"} cm-hide-inputs{/if}" enctype="multipart/form-data">
<input type="hidden" name="staff_id" value="{$id}" />

<fieldset>
	<div class="control-group">
	    <label class="control-label" for="staff_first_name_{$id}">{__("staff_first_name")}:</label>
	    <div class="controls">
	        <input type="text" name="staff_data[first_name]" id="staff_first_name_{$id}" value="{$staff.first_name}" class="span9" />
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label" for="staff_last_name_{$id}">{__("staff_last_name")}:</label>
	    <div class="controls">
	        <input type="text" name="staff_data[last_name]" id="staff_last_name_{$id}" value="{$staff.last_name}" class="span9" />
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label" for="staff_email_{$id}">{__("staff_email")}:</label>
	    <div class="controls">
	        <input type="text" name="staff_data[email]" id="staff_email_{$id}" value="{$staff.email}" class="span9" />
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label cm-required" for="staff_function_{$id}">{__("staff_function")}:</label>
	    <div class="controls">
	    	<textarea name="staff_data[function]" id="staff_function_{$id}" class="span9">{$staff.function}</textarea>
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label" for="staff_photo_{$id}">{__("staff_photo")}:
	    	<a class="cm-tooltip" title="{__("staff_photo_help")}"><i class="icon-question-sign"></i></a>
	    </label>
	    <div class="controls">
	    	{*if $staff.photo }
	    		<img src="{$staff.photo_data.http_photo_path}" width="64" height="64" border="0" />
	    	{/if*}
			<input type="text" name="staff_data[photo]" id="staff_photo_{$id}" value="{$staff.photo}" class="span3" />
	    	<input type="file" name="staff_data[upload]" id="staff_upload_{$id}" accept="image/jpeg,image/png,image/gif" value=""
	    		onchange="$('#staff_photo_{$id}').val(this.value.replace(/.*[\/\\]/, ''))" />
	    </div>
	</div>	
	<div class="control-group">
	    <label class="control-label" for="staff_user_id_{$id}">{__("staff_user_id")}:</label>
	    <div class="controls">
	    	<input type="text" name="staff_data[user_id]" id="staff_user_id_{$id}" value="{$staff.user_id}" class="span9" />
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label cm-required" for="staff_orderby_{$id}">{__("position")}:</label>
	    <div class="controls">
	    	<input type="text" name="staff_data[orderby]" id="staff_orderby_{$id}" value="{if $staff.orderby}{$staff.orderby}{else}{$max_pos}{/if}" class="span9" />
	    </div>
	</div>
</fieldset>

{if !$hide_inputs}
	<div class="buttons-container">
	    {include file="buttons/save_cancel.tpl" but_name="dispatch[staff.update]" cancel_action="close" save=$id}
	</div>
{/if}
</form>